# Bot Discord

Bot Discord utilisant le moteur de recherche de Google.

## Description

Ce projet a pour but de construire un bot Discord dans le langage de programmation Java en utilisant la librairie [JDA (Java Discord API)](https://github.com/DV8FromTheWorld/JDA).
Ce bot utilise les services de recherche de Google en utilisant la librairie [SerpAPI](https://serpapi.com/).

Voici les fonctionnalités de ce bot:
* Faire la définition d'un mot.
* Traduire en anglais un mot ou une phrase courte en français ou vice-versa.
* Convertir une unité de mesure vers une autre unité de mesure.

## Logiciels nécessaires

* Discord https://discord.com/download
* JDK 16 Temurin https://adoptium.net/temurin/releases/?version=16
* Gradle https://gradle.org/

## Clé SerpAPi
Afin d'exécuter le programme, il est nécessaire d'avoir, entre autre, une clé de l'API SerpAPI
1. Accédez au site Web de [SerpApi](https://serpapi.com/) et inscrivez-vous pour obtenir un compte.
    * Vous aurez besoin d'une adresse courriel valide pour créer un compte SerpAPi, car un code de vérification sera envoyé à l'adresse courriel.
    * Vous aurez également besoin d'un numéro de téléphone valide, car un code de vérification sera envoyé au numéro. Vous pouvez utiliser ce site pour utiliser des numéros de téléphones temporaire: https://quackr.io/
2. Une fois que vous êtes connecté à votre compte SerpApi, accédez à l'onglet "Api Key" dans le menu de navigation à gauche.
3. Copiez votre clé privée, elle vous sera utile durant l'exécution du programme, donc sauvegardez-la quelque part

## Serveur Discord et GuildId
Vous devez avoir un serveur Discord pour votre bot Discord
1. À partir de l'un de votre compte Discord, ajoutez un serveur Discord, voir ce lien si vous ne savez pas comment: https://www.ionos.fr/digitalguide/serveur/know-how/creer-un-serveur-discord/
2. Sur l'application Discord, assurez-vous d'activer le mode développeur, voir ce lien si vous ne savez pas comment: https://wiki.discord-france.fr/utilisateur/parametres-application/mode-developpeur
3. Faites un click droit sur votre serveur Discord et cliquez sur "Copier l'identifiant", ceci est votre GuildId. Il vous sera utile pour l'exécution du programme, donc sauvegardez-le quelque part

## Création du Bot pour votre serveur Discord
Vous avez besoin de créer une application sur Discord pour votre Bot Discord afin de récupérer son jeton 
1. Accédez à ce lien: https://discord.com/developers/applications
2. Cliquez sur "New Application" en haut à droite
3. Créer un nom à votre application et cliquez sur "Create"
4. Allez dans la section "Bot" et appuyez sur "Add Bot"
5. Cliquez sur "Copy" en dessous de "token" pour copier votre jeton, elle vous sera utile pour l'exécution du programme, donc sauvegardez le quelque part
6. Allez dans l'onglet "OAuth2", puis dans l'onglet "URL Generator" dans le menu de navigation à gauche et sélectionnez "bot" dans la fenetres "scope"
7. Mettre "Administrator" comme Bot Permissions et copier le "Generated Url"
8. Coller le "Generated Url" dans votre navigateur Web et sélectionnez votre serveur Discord pour ajouter le Bot sur votre serveur

## Guide de compilation et exécution
Soyez sûre d'avoir votre clé SerpAPi, votre GuildId de votre serveur Discord ainsi que le jeton du Bot Discord
1. Clonez le dépôt sur votre machine locale. Vous pouvez le faire en utilisant la commande suivante dans votre terminal:
```
git clone https://gitlab.com/kmessier/discord-bot
```
Si vous n'avez pas git d'installer sur votre ordinateur, vous pouvez l'installer ici: https://git-scm.com/downloads

2. Accédez au répertoire du projet que vous venez de cloner et créez les dossiers et fichiers suivants dans le dossier /discord-bot/src/: 
    * ressources/discord/guildId.txt (Mettre votre GuildId de votre serveur Discord à la première ligne de ce fichier)
    * ressources/discord/token.txt (Mettre votre Jeton de votre Bot Discord à la première ligne de ce fichier)
    * ressources/serpAPI/serpAPIKey.txt (Mettre votre clé de votre compte SerpAPi  à la première ligne de ce fichier)

3. Ouvrez votre terminal et accédez au répertoire du projet Discord Bot que vous avez cloner et allez dans le répertoire discord-bot en utilisant la commande suivante : 
```
cd discord-bot/discord-bot
```
4. Assurez-vous que vous avez JDK 16 Temurin d'installé sur votre ordinateur. Vous pouvez l'installer ici: https://adoptium.net/temurin/releases/?version=16

5. Assurez-vous que Gradle est installé sur votre ordinateur. Vous pouvez vérifier en exécutant la commande suivante dans votre terminal :
```
gradle -v
```
 Si Gradle n'est pas installé, vous pouvez télécharger l'installateur à partir du site officiel de [Gradle](https://gradle.org/)

6. Exécutez la commande suivante dans votre terminal afin de compiler et exécuter le programme:
```
./gradlew run
```
Si vous voulez arrêter le programme, il suffit de faire les touches Ctrl + C et répondre « O ». 
 
Si vous voulez simplement compiler le programme, exécutez la commande suivante:
```
./gradlew assemble
```

7. Votre bot Discord est maintenant en cours d'exécution ! Vous pouvez désormais utiliser les commandes du Bot sur votre serveur Discord.


## Commandes du bot

* /definition [mot]
* /traduire [atraduire] [langueutilisee] [languetraduit]
* /convertir [nombre] [unitemesure] [unitemesurevoulue]

## Tests

1. Ouvrez votre terminal et accédez au répertoire du projet Discord Bot que vous avez cloner et allez dans le répertoire discord-bot en utilisant la commande suivante : 
```
cd discord-bot/discord-bot
```
2. Exécutez la commande suivante dans votre terminal afin d'exécuter les tests:
```
./gradlew test

```
Si les tests sont déjà à jour et que vous souhaitez quand même voir le log des tests ayant passés, vous pouvez effectuer cette commande pour exécuter à nouveau les tests:
```
./gradlew cleanTest test

```

## Communication
Voici le lien de mon serveur Discord, si vous souhaitez communiquez avec moi: https://discord.gg/8gWKVH4jpT

## Auteur
Kevin Messier

## License
MIT License

Copyright (c) 2023 Kevin Messier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
