package discordBot;

import enums.Commande;
import enums.OptionCommande;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import org.jetbrains.annotations.NotNull;
import serpAPI.IGestionSerpAPI;

import java.util.Objects;

/**
 * Classe permettant au Bot d'intéragir en fonction des différentes commandes
 *
 * @author Kevin Messier
 * @version %I% %G%
 *
 */
public class BotCommandes extends ListenerAdapter {
    /**
     * Permet de faire le pont entre la classe DiscordBot et la classe SerpAPI
     */
    private final IGestionSerpAPI gestionSerpAPI;

    /**
     * Initialise le BotCommandes
     *
     * @param gestionSerpAPI Permet de faire le pont entre la classe DiscordBot et la classe SerpAPI
     */
    public BotCommandes(IGestionSerpAPI gestionSerpAPI) {
        this.gestionSerpAPI = gestionSerpAPI;
    }

    /**
     * Appelé lorsqu'un utilisateur exécute une commande slash sur le serveur Discord
     *
     * @param event Évènement enclanché lors d'une commande slash exécuté sur le serveur Discord
     */
    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
        if (event.getName().equals(Commande.DEFINITION.transformerEnMinuscule())) {
            AfficherDefinition(event);
        } else if (event.getName().equals(Commande.TRADUIRE.transformerEnMinuscule())) {
            AfficherTraduction(event);
        } else if (event.getName().equals(Commande.CONVERTIR.transformerEnMinuscule())) {
            AfficherConvertion(event);
        }
    }


    /**
     * Permet au Bot d'afficher le résultat de la commande /definition
     *
     * @param event L'évènement enclanché par une commande slash sur le serveur Discord
     */
    private void AfficherDefinition(@NotNull SlashCommandInteractionEvent event) {
        OptionMapping motOption = event.getOption(OptionCommande.MOT.transformerEnMinuscule());

        if (motOption == null || motOption.getAsString().equals("")) {
            event.reply("Aucun mot n'a été spécifié pour la recherche").queue();
        } else {
            String[] mots = motOption.getAsString().trim().split(" ");
            if (mots.length == 1) {
                String recherche = "\"Définition " + motOption.getAsString() + "\"";

                event.deferReply().queue();
                String resultatObtenu = ObtenirResultatRecherche(recherche,
                        Commande.DEFINITION.transformerEnMinuscule());
                if (resultatObtenu == null) {
                    recherche = "Définition " + motOption.getAsString();
                    resultatObtenu = ObtenirResultatRecherche(recherche,
                            Commande.DEFINITION.transformerEnMinuscule());
                }
                event.getHook().sendMessage(Objects.requireNonNullElse(resultatObtenu,
                        "Définition introuvable")).queue();
            } else {
                event.reply("Vous devez entrez un seul mot à définir").queue();
            }
        }
    }

    /**
     * Permet au Bot d'afficher le résultat de la commande /traduction
     *
     * @param event L'évènement enclanché par une commande slash sur le serveur Discord
     */
    private void AfficherTraduction(@NotNull SlashCommandInteractionEvent event) {
        OptionMapping aTraduireOption = event.getOption(OptionCommande.ATRADUIRE.transformerEnMinuscule());
        OptionMapping langueUtiliseeOption = event.getOption(OptionCommande.LANGUEUTILISEE.transformerEnMinuscule());
        OptionMapping langueTraduitOption = event.getOption(OptionCommande.LANGUETRADUIT.transformerEnMinuscule());

        if (aTraduireOption == null || aTraduireOption.getAsString().equals("") ||
                langueUtiliseeOption == null || langueUtiliseeOption.getAsString().equals("") ||
                langueTraduitOption == null || langueTraduitOption.getAsString().equals("")) {
            event.reply("Les options atraduire, langueutilisee et languetraduit doivent être spécifiées")
                    .queue();
        } else if (langueUtiliseeOption.getAsString().equals(langueTraduitOption.getAsString())) {
            event.reply("Les options langueutilisee et languetraduit doivent être différents").queue();
        } else {
            String recherche = "Translate " + aTraduireOption.getAsString() + " from " +
                    langueUtiliseeOption.getAsString() + " to " + langueTraduitOption.getAsString();

            event.deferReply().queue();
            String resultatObtenu = ObtenirResultatRecherche(recherche, Commande.TRADUIRE.transformerEnMinuscule());
            event.getHook().sendMessage(Objects.requireNonNullElse(resultatObtenu,
                    "Traduction impossible")).queue();
        }
    }

    /**
     * Permet au Bot d'afficher le résultat de commande /convertion
     *
     * @param event L'évènement enclanché par une commande slash sur le serveur Discord
     */
    private void AfficherConvertion(@NotNull SlashCommandInteractionEvent event) {
        OptionMapping nombreOption = event.getOption(OptionCommande.NOMBRE.transformerEnMinuscule());
        OptionMapping uniteMesureOption = event.getOption(OptionCommande.UNITEMESURE.transformerEnMinuscule());
        OptionMapping uniteMesurevoulueOption =
                event.getOption(OptionCommande.UNITEMESUREVOULUE.transformerEnMinuscule());

        if (nombreOption == null || uniteMesureOption == null || uniteMesureOption.getAsString().equals("")  ||
                uniteMesurevoulueOption == null || uniteMesurevoulueOption.getAsString().equals("")) {
            event.reply("Les options nombre, unitemesure et unitemesurevoulue doivent être spécifiées")
                    .queue();
        } else if (nombreOption.getAsInt() <= 0) {
            event.reply("Le nombre doit être plus grand que 0").queue();
        } else if (uniteMesureOption.getAsString().equals(uniteMesurevoulueOption.getAsString())) {
            event.reply("Les options unitemesure et unitemesurevoulue doivent être différents").queue();
        } else {
            String recherche = nombreOption.getAsInt() + " " + uniteMesureOption.getAsString() + " to " +
                    uniteMesurevoulueOption.getAsString();

            event.deferReply().queue();
            String resultatObtenu = ObtenirResultatRecherche(recherche, Commande.CONVERTIR.transformerEnMinuscule());
            event.getHook().sendMessage(Objects.requireNonNullElse(resultatObtenu,
                    "Convertion impossible")).queue();
        }
    }

    /**
     * Obtient le résultat de la recherche Google
     *
     * @param recherche La recherche Google a effectuer
     * @param typeCommande Le type de la commande utilisé par l'utilisateur sur le serveur Discord
     * @return Le résultat de la recherche Google
     */
    private String ObtenirResultatRecherche(String recherche, String typeCommande)
    {
        return gestionSerpAPI.ObtenirResultatSerpAPI(recherche, typeCommande);
    }
}
