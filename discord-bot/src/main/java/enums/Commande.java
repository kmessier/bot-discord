package enums;

/**
 * Enum pour les commandes du bot Discord
 *
 * @author Kevin Messier
 * @version %I% %G%
 */
public enum Commande {
    /**
     * Représente la commande /definition du bot Discord
     */
    DEFINITION {
        /**
         * {@inheritDoc}
         */
        @Override
        public String transformerEnMinuscule() {
            return DEFINITION.toString().toLowerCase();
        }
    },
    /**
     * Représente la commande /convertir du bot Discord
     */
    CONVERTIR {
        /**
         * {@inheritDoc}
         */
        @Override
        public String transformerEnMinuscule() {
            return CONVERTIR.toString().toLowerCase();
        }
    },
    /**
     * Représente la commande /traduire du bot Discord
     */
    TRADUIRE {
        /**
         * {@inheritDoc}
         */
        @Override
        public String transformerEnMinuscule() {
            return TRADUIRE.toString().toLowerCase();
        }
    }
    ;

    /**
     * Permet de transformer l'enum en une chaîne de caractère en minuscule
     *
     * @return L'enum transformé en une chaîne de caractère en minuscule
     */
    public abstract String transformerEnMinuscule();
}
