package enums;

/**
 * Enum pour les options des commandes du Bot discord
 *
 * @author Kevin Messier
 * @version %I% %G%
 */
public enum OptionCommande {
    /**
     * Représente l'option [mot] de la commande /definition
     */
    MOT {
        /**
         * {@inheritDoc}
         */
        @Override
        public String transformerEnMinuscule() {
            return MOT.toString().toLowerCase();
        }
    },
    /**
     * Représente l'option [atraduire] de la commande /traduire
     */
    ATRADUIRE {
        /**
         * {@inheritDoc}
         */
        @Override
        public String transformerEnMinuscule() {
            return ATRADUIRE.toString().toLowerCase();
        }
    },
    /**
     * Représente l'option [langueutilisee] de la commande /traduire
     */
    LANGUEUTILISEE {
        /**
         * {@inheritDoc}
         */
        @Override
        public String transformerEnMinuscule() {
            return LANGUEUTILISEE.toString().toLowerCase();
        }
    },
    /**
     * Représente l'option [languetraduit] de la commande /traduire
     */
    LANGUETRADUIT {
        /**
         * {@inheritDoc}
         */
        @Override
        public String transformerEnMinuscule() {
            return LANGUETRADUIT.toString().toLowerCase();
        }
    },
    /**
     * Représente l'option [nombre] de la commande /convertir
     */
    NOMBRE {
        /**
         * {@inheritDoc}
         */
        @Override
        public String transformerEnMinuscule() {
            return NOMBRE.toString().toLowerCase();
        }
    },
    /**
     * Représente l'option [unitemesure] de la commande /convertir
     */
    UNITEMESURE {
        /**
         * {@inheritDoc}
         */
        @Override
        public String transformerEnMinuscule() {
            return UNITEMESURE.toString().toLowerCase();
        }
    },
    /**
     * Représente l'option [unitemesurevoulue] de la commande /convertir
     */
    UNITEMESUREVOULUE {
        /**
         * {@inheritDoc}
         */
        @Override
        public String transformerEnMinuscule() {
            return UNITEMESUREVOULUE.toString().toLowerCase();
        }
    }
    ;

    /**
     * Permet de transformer l'enum en une chaîne de caractère en minuscule
     *
     * @return L'enum transformé en une chaîne de caractère en minuscule
     */
    public abstract String transformerEnMinuscule();
}
