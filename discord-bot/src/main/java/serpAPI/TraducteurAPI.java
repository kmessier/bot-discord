package serpAPI;

import com.google.gson.JsonObject;
import serpapi.GoogleSearch;
import serpapi.SerpApiSearchException;

import java.util.Map;

/**
 * Classe permettant d'aller chercher sur l'API SerpAPI la traduction correspondante à la recherche
 *
 * @author Kevin Messier
 * @version %I% %G%
 */
public class TraducteurAPI extends SerpAPI {

    /**
     * {@inheritDoc}
     */
    @Override
    public String ObtenirResultatRecherche(Map parametreRecherche) {
        GoogleSearch rechercheGoogle = CreerGoogleRecherche(parametreRecherche);

        String resultatRecherche = null;
        try {
            JsonObject donnees = rechercheGoogle.getJson();
            resultatRecherche = donnees.get("answer_box").getAsJsonObject().get("translation").getAsJsonObject()
                    .get("target").getAsJsonObject().get("text").getAsString();
        } catch (SerpApiSearchException | NullPointerException exception) {
            exception.printStackTrace();
        }
        return resultatRecherche;
    }
}
