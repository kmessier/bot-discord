package serpAPI;

import configs.ISerpAPIConfiguration;
import enums.Commande;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe permettant de faire le pont entre la classe Programme et la classe SerpAPi
 *
 * @author Kevin Messier
 * @version %I% %G%
 *
 */
public class GestionSerpAPI implements IGestionSerpAPI {

    /**
     * La Key de l'API SerpAPI
     */
    private final String key;

    /**
     * Initialise le GestionSerpAPI
     *
     * @param serpAPIConfiguration La configuration de l'API SerpAPI
     */
    public GestionSerpAPI(ISerpAPIConfiguration serpAPIConfiguration) {
        key = serpAPIConfiguration.getSerpAPIKey();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String ObtenirResultatSerpAPI(String recherche, String typeCommande) {
        Map<String, String> parametreRecherche = CreerHashMapSerpAPI(recherche);
        String resultatRechercheSerpAPI = "";

        if (typeCommande.equals(Commande.DEFINITION.transformerEnMinuscule())) {
            DefinisseurAPI definisseurAPI = new DefinisseurAPI();
            resultatRechercheSerpAPI = definisseurAPI.ObtenirResultatRecherche(parametreRecherche);
        } else if (typeCommande.equals(Commande.TRADUIRE.transformerEnMinuscule())) {
            TraducteurAPI traducteurAPI = new TraducteurAPI();
            resultatRechercheSerpAPI = traducteurAPI.ObtenirResultatRecherche(parametreRecherche);
        } else if (typeCommande.equals(Commande.CONVERTIR.transformerEnMinuscule())) {
            ConvertisseurAPI convertisseurAPI = new ConvertisseurAPI();
            resultatRechercheSerpAPI = convertisseurAPI.ObtenirResultatRecherche(parametreRecherche);
        }

        return resultatRechercheSerpAPI;
    }

    /**
     * Permet de créer le HashMap utilisé pour la recherche Google avec SerpAPI
     *
     * @param recherche La recherche Google a effectuer
     * @return Le Map pour la recherche Google avec SerpAPI
     */
    private Map<String, String> CreerHashMapSerpAPI(String recherche)
    {
        Map<String, String> parametre = new HashMap<>();
        parametre.put("q", recherche);
        parametre.put("location", "Canada");
        parametre.put("hl", "en");
        parametre.put("gl", "ca");
        parametre.put("google_domain", "google.ca");
        parametre.put("api_key", key);

        return parametre;
    }
}
