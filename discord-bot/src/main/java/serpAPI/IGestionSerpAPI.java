package serpAPI;

/**
 * Interface permettant de faire le pont entre la classe DiscordBot et la classe SerpAPi
 *
 * @author Kevin Messier
 * @version %I% %G%
 *
 */
public interface IGestionSerpAPI {
    /**
     * Permet d'obtenir le resultat de la recherche Google à l'aide de SerpAPI
     *
     * @param recherche La recherche Google à effectuer
     * @param typeCommande Le type de la commande utilisé par l'utilisateur sur le serveur Discord
     * @return Le résultat de la recherche Google effectuée par SerpAPI
     */
     String ObtenirResultatSerpAPI(String recherche, String typeCommande);
}
