import configs.Configuration;
import discordBot.DiscordBot;
import serpAPI.GestionSerpAPI;

/**
 * Programme principal du bot Discord
 *
 * @author Kevin Messier
 * @version %I% %G%
 *
 */
public class Programme {
    /**
     * Exécution du programme
     *
     * @param args Les paramètres du programme
     */
    public static void main(String[] args) {
        try {
            Configuration config = new Configuration();
            GestionSerpAPI gestionSerpAPI = new GestionSerpAPI(config);
            DiscordBot discordBot = new DiscordBot(config, gestionSerpAPI);
            discordBot.Demarrer();

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
