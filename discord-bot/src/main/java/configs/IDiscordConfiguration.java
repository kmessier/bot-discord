package configs;

/**
 * Interface pour les configurations du serveur Discord
 *
 * @author Kevin Messier
 * @version %I% %G%
 *
 */
public interface IDiscordConfiguration {
    /**
     * Permet d'obtenir le Token du serveur Discord
     *
     * @return Le Token du serveur Discord
     */
    String getDiscordToken();

    /**
     * Permet d'obtenir le GuildId du serveur Discord
     *
     * @return Le GuildId du serveur Discord
     */
    String getDiscordGuildId();
}
