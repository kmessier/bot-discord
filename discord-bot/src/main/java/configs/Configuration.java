package configs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Classe représentant la configuration du programme
 *
 * @author Kevin Messier
 * @version %I% %G%
 *
 */
public class Configuration implements IDiscordConfiguration, ISerpAPIConfiguration {

    /**
     * Le Token du serveur Discord
     */
    private final String discordToken;

    /**
     * La Key de SerpAPi
     */
    private final String serpAPIKey;

    /**
     * Le GuildId du serveur Discord
     */
    private final String discordGuildId;

    /**
     * Initialisation de la Configuration
     *
     * @throws IOException Lorsqu'une I/O exception se produit
     */
    public Configuration() throws IOException {
        Path discordTokenPath = Paths.get("src/ressources/discord/token.txt");
        Path discordGuildIdPath = Paths.get("src/ressources/discord/guildId.txt");
        Path serpApiKeyPath = Paths.get("src/ressources/serpAPI/serpAPIKey.txt");
        try {
            discordToken = Files.readAllLines(discordTokenPath).get(0);
            discordGuildId = Files.readAllLines(discordGuildIdPath).get(0);
            serpAPIKey = Files.readAllLines(serpApiKeyPath).get(0);
        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            System.err.println("Une erreur s'est produite lors de la lecture des fichiers de configurations.");
            throw indexOutOfBoundsException;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDiscordToken() {
        return discordToken;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDiscordGuildId() {
        return discordGuildId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSerpAPIKey() {
        return serpAPIKey;
    }
}
