package configs;

/**
 * Interface pour les configurations de l'API SerpAPI
 *
 * @author Kevin Messier
 * @version %I% %G%
 *
 */
public interface ISerpAPIConfiguration {
    /**
     * Permet d'obtenir la Key de SerpAPI
     *
     * @return La Key de SerpAPI
     */
    String getSerpAPIKey();
}
